module Main (main) where

import Curry.Entrypoint (appMain)

main ∷ IO ()
main = appMain
