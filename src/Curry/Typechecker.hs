module Curry.Typechecker where

import           Common
import           Common.Polysemy
import           Curry.Syntax
import qualified Data.HashMap.Strict as H
import qualified Data.HashSet        as HS

type TypeError = Text

data VarInfo = VarInfo {
  _varType   ∷ Maybe Type
  , _varBind ∷ BindSite
  }
  deriving (Show)
makeLenses ''VarInfo

type VarTable = HashMap Iden VarInfo

newtype Context = Context {_unContext ∷ VarTable}
  deriving (Show, Semigroup, Monoid)
makeLenses ''Context

-- | The "inference state" of a variable during type checking.
-- 'InScope Nothing' means that the variable is in scope but has not been assigned a type yet.
data VarState =
  InScope VarInfo
  | NotInScope
  deriving (Show)

runCheckModule ∷ ModuleDef 'Parsed → Either TypeError (ModuleDef 'Checked)
runCheckModule m = do
  x ← runFinal $ runError (checkModule m)
  case x of
    Right m' → return m'
    Left err → Left err

emptyTypesTable ∷ TypesTable
emptyTypesTable = TypesTable mempty mempty mempty

buildTypesTable ∷ ∀ r. Members '[Error TypeError] r ⇒ [DataDef] → Sem r TypesTable
buildTypesTable = foldM addDef emptyTypesTable
  where
  addDef ∷ TypesTable → DataDef → Sem r TypesTable
  addDef tbl DataDef{_rhs = SumType{..}, ..} = do
    addType tbl _name >>= \tbl' → foldM (addConstructor _name) tbl' _constructors
    where
    addType t@TypesTable{..} _name =
      case insertIfNotThere _name _constructors _typesMap of
        Nothing → throw $ "Duplicate type: " <> _name
        Just n  → return (set typesMap n t)
    addConstructor ∷ Iden → TypesTable → ConstructorDef → Sem r TypesTable
    addConstructor tyName t@TypesTable{..} def@ConstructorDef {..} =
      case insertIfNotThere _constructorName tyName _constructorsType of
        Nothing → throw $ "Duplicate constructor: " <> _constructorName
        Just n  → return (over constructorsDef (H.insert _constructorName def) (set constructorsType n t))

checkModule ∷ Members '[Error TypeError] r ⇒ ModuleDef 'Parsed → Sem r (ModuleDef 'Checked)
checkModule m@ModuleDef{..} = do
  typesTable ← buildTypesTable _typeDefs
  runReader typesTable $
    evalState mempty $ do
    checkedFuns ← mapM checkFunDef _funDefs
    let _extra = typesTable
        checkedModule = ModuleDef{_funDefs = checkedFuns, ..}
    checkMainExists checkedModule
    return checkedModule

globalBind ∷ Members '[Error TypeError, State Context] r ⇒ Iden → Maybe Type → Sem r ()
globalBind var ty = do
  r ← insertIfNotThere var (VarInfo ty BindTopFun) <$> gets _unContext
  case r of
    Nothing → throw $ "Identifier already bound:" <> var
    Just x  → put $ Context x

localBind' ∷ Members '[State Context] r ⇒ Iden → VarInfo → Sem r ()
localBind' var vi =
  modify (over unContext (H.insert var vi))

lookupTypeIden ∷ Members '[Error TypeError, Reader TypesTable] r ⇒ Iden → Sem r ()
lookupTypeIden i = unlessM (asks (H.member i . _typesMap)) (throw $ "Type not defined: " <> i)

checkType ∷ Members '[Error TypeError, Reader TypesTable] r ⇒ Type → Sem r ()
checkType t = case t of
  TyDataDef i → lookupTypeIden i
  UnitTy      → return ()
  FunTy a b   → mapM_ checkType [a, b]
  StringTy    → return ()

saveVarType ∷ Members '[State Context] r ⇒ Iden → Type → Sem r ()
saveVarType fun ty =
  modify (set (unContext . at fun . _Just . varType) (Just ty))

checkFunDef ∷ Members '[Error TypeError, Reader TypesTable, State Context] r ⇒ FunDef 'Parsed → Sem r (FunDef 'Checked)
checkFunDef FunDef{..} = do
  whenJust _funType checkType
  globalBind _funName _funType
  typedExpr ← fst <$> withLocalContext (checkExpr _funType _funExpr)
  when (isNothing _funType) (saveVarType _funName (_exprType typedExpr))
  return FunDef{_funExpr = typedExpr, ..}


checkExpr ∷ Members '[Error TypeError, Reader TypesTable,  State Context] r
  ⇒ Maybe Type → Expr 'Parsed → Sem r TypedExpr
checkExpr t e = case t of
  Just ty → checkExprType ty e
  Nothing → inferExprType' e

-- | The given variable must be in scope.
-- If it has no type in the context, the given type is assigned.
-- If it already has a type in the context, it checks that it matches.
checkVariable ∷ Members '[Error TypeError, State Context] r ⇒
  Iden → Type → Sem r TypedExpr
checkVariable var ty = do
  VarInfo{..} ← lookupVarInScope var
  case _varType of
    Nothing  → saveVarType var ty
    Just ty' → unless (ty == ty') (eBadType (Var var ()) ty ty')
  return (TypedExpr (Var var _varBind) ty)


eBadType ∷ Members '[Error TypeError] r ⇒ Expr 'Parsed → Type → Type → Sem r a
eBadType e good bad = throw ("Expression " <> showT e <> " was expected to have type " <> showT good
                   <> " but has type " <> showT bad)

eBadTypeFun ∷ Members '[Error TypeError] r ⇒ Expr 'Parsed → Sem r a
eBadTypeFun e = throw ("Expression " <> showT e <> " was expected to have a function type.")

badLambdaArgTy ∷ Members '[Error TypeError] r ⇒ Iden → Type → Type → Sem r a
badLambdaArgTy v expec bad = throw ("Lambda argument " <> v <> " was expected to have type " <> showT expec
                                   <> " but has type " <> showT bad)

checkMainExists ∷ ∀ r. Members '[Error TypeError] r
   ⇒ ModuleDef 'Checked → Sem r ()
checkMainExists ModuleDef{..} =
  case _exprType . _funExpr <$> find ((== "main") . _funName) _funDefs of
    Just UnitTy → return ()
    Just {}     → throw "the main function must have type ()"
    Nothing     → throw "function definition main not found"


-- | Checks that an expression has a specific type.
checkExprType ∷ ∀ r. Members '[Error TypeError, Reader TypesTable, State Context] r
  ⇒ Type → Expr 'Parsed → Sem r TypedExpr
checkExprType t e = case e of
  Unit        → checkUnit
  Var v ()    → checkVariable v t
  Constr c    → checkConstr c
  Lambda{..}  → checkLambda _lamVar _lamVarType _lamExpr
  Case ce as  → checkCase ce as
  App a b     → checkApp a b
  StringLit t → checkString t
  where
  badType ∷ Type → Sem r ()
  badType = eBadType e t
  checkUnit ∷ Sem r TypedExpr
  checkUnit = TypedExpr Unit UnitTy <$ unless (t == UnitTy) (badType UnitTy)
  checkString ∷ Text → Sem r TypedExpr
  checkString x = TypedExpr (StringLit x) StringTy <$ unless (t == StringTy) (badType StringTy)
  checkConstr ∷ Iden → Sem r TypedExpr
  checkConstr c = do
    t' ← lookupConstrType c
    unless (t == t') (badType t')
    return (TypedExpr (Constr c) t)
  checkLambda ∷ Iden → Maybe Type → Expr 'Parsed → Sem r TypedExpr
  checkLambda arg margty body = case t of
    FunTy argty' retty → do
      checkLambdaArg argty' arg margty
      let varInfo = VarInfo (Just argty') BindLambdaArg
      typedBody ← fst <$> withLocalBind arg varInfo (checkExprType retty body)
      return (TypedExpr (Lambda arg margty typedBody) t)
    _ → throw ("The expression " <> showT e <> " cannot have type " <> showT t)
  checkLambdaArg ∷ Type → Iden → Maybe Type → Sem r ()
  checkLambdaArg expected arg margty = case margty of
    Nothing    → return ()
    Just argty → unless (argty == expected) (badLambdaArgTy arg expected argty)
  checkCase ∷ Expr 'Parsed → [CaseAlt 'Parsed] → Sem r TypedExpr
  checkCase cexpr alts = do
    cExpr ← checkCExprAndPats cexpr alts
    cAlts ← mapM (checkCaseAltBody t) alts
    return (TypedExpr (Case cExpr cAlts) t)
  -- | NOTE this is a conflictive case: We require the function term 'f' to be inferable by itself.
  -- Note that we are ignoring that we know that the type of 'f' is of the form (? → t).
  -- Using a unification algorithm would greatly improve the inference algorithm.
  checkApp ∷ Expr 'Parsed → Expr 'Parsed → Sem r TypedExpr
  checkApp f arg = do
    fty ← _exprType <$> inferExprType' f
    case fty of
      FunTy argty retty → do
        checkExprType argty arg
        unless (t == retty) (eBadType e t retty)
        checkExprType argty arg
      _ → eBadTypeFun e

checkCaseAltBody ∷ ∀ r. Members '[Error TypeError, Reader TypesTable, State Context] r ⇒
  Type → CaseAlt 'Parsed → Sem r (CaseAlt 'Checked)
checkCaseAltBody t CaseAlt{..} = do
  varTys ← snd <$> inferPatType _altPat
  altBody ← fst <$> withLocalBinds varTys (checkExprType t _altExpr)
  return CaseAlt{_altExpr = altBody, ..}

lookupVar ∷ Members '[State Context] r ⇒ Iden → Sem r VarState
lookupVar var = maybe NotInScope InScope <$> gets @Context (H.lookup var . _unContext)

insertVar ∷ Members '[State Context] r ⇒ Iden → VarState → Sem r ()
insertVar v vstate = case vstate of
  InScope mt →  modify (over unContext (H.insert v mt))
  NotInScope →  modify (over unContext (H.delete v))

-- | Returns Just t when the variable is in scope and has type t.
-- Returns Nothing when the variable is in scope but has no type yet.
-- Fails when the variable is not in scope
lookupVarInScope ∷ ∀ r. Members '[Error TypeError, State Context] r ⇒ Iden → Sem r VarInfo
lookupVarInScope var = do
  vs ← lookupVar var
  case vs of
    InScope mt → return mt
    NotInScope → throw $ "Variable not in scope: " <> var

lookupConstrRetType ∷ ∀ r. Members '[Error TypeError, Reader TypesTable] r ⇒ Iden → Sem r Type
lookupConstrRetType c =
  TyDataDef <$> fromMaybeM (throw ("Constructor not in scope: " <> c)) (asks (H.lookup c . _constructorsType))

lookupConstrType ∷ ∀ r. Members '[Error TypeError, Reader TypesTable] r ⇒ Iden → Sem r Type
lookupConstrType c = do
  ret ← lookupConstrRetType c
  args ← lookupConstrArgs c
  return (funTyFromSig args ret)

lookupConstrArgs ∷ ∀ r. Members '[Error TypeError, Reader TypesTable] r ⇒ Iden → Sem r [Type]
lookupConstrArgs c =
  _constructorArgs <$> fromMaybeM (throw ("Constructor not in scope: " <> c)) (asks (H.lookup c . _constructorsDef))


lookupType ∷ ∀ r. Members '[Error TypeError, Reader TypesTable] r ⇒ Iden → Sem r [ConstructorDef]
lookupType t =
  fromMaybeM (throw ("Type not in scope: " <> t)) (asks (H.lookup t . _typesMap))

withLocalContext ∷ ∀ r a. Members '[State Context] r ⇒ Sem r a → Sem r (a, Context)
withLocalContext action = do
  s ← get
  res ← action
  s' ← get
  put s
  return (res, s')

withLocalBinds ∷ ∀ r a. Members '[State Context] r ⇒ [(Iden, VarInfo)] → Sem r a → Sem r (a, [Maybe Type])
withLocalBinds varsTys action = do
  varStates ← mapM lookupVar vars
  mapM_ (uncurry localBind') varsTys
  res ← action
  varStates' ← mapM lookupVar vars
  mapM_ (uncurry insertVar) (zip vars varStates)
  return (res, map (_varType . fromInScope) varStates')
  where
  vars = map fst varsTys
  fromInScope (InScope x) = x
  fromInScope _           = error "impossible"

withLocalBind ∷ Members '[State Context] r ⇒ Iden → VarInfo → Sem r a → Sem r (a, Maybe Type)
withLocalBind var varInfo action =
  second head <$> withLocalBinds [(var, varInfo)] action

inferPatType ∷ Members '[Error TypeError, Reader TypesTable] r ⇒ Pat → Sem r (Type, [(Iden, VarInfo)])
inferPatType p = case p of
  PUnit         → return (UnitTy, [])
  PApp _bindConstructorName vars → do
    unless (allDiferent vars) (throw ("Repeated variables in pattern " <> showT p))
    cty ← lookupConstrRetType _bindConstructorName
    argstys ← lookupConstrArgs _bindConstructorName
    unless (length vars == length argstys) (throw $ "Wrong number of arguments in pattern " <> showT p)
    return (cty, [ (var, VarInfo (Just ty) b)
                 | (_argumentIx, var, ty) ← zip3 [0..] vars argstys
                 , let b = BindPattern {..} ] )

checkPatType ∷ Members '[Error TypeError, Reader TypesTable] r ⇒ Type → Pat → Sem r ()
checkPatType ty p = unlessM ((== ty) . fst <$> inferPatType p) $
  throw ("Pattern " <> showT p <> " expected of type " <> showT ty)


inferExprType' ∷ ∀ r. Members '[Error TypeError, Reader TypesTable, State Context] r
  ⇒ Expr 'Parsed → Sem r TypedExpr
inferExprType' e = fromMaybeM (throw $ "Unable to infer type of " <> showT e) (inferExprType e)

-- | Infers the type of an expression.
inferExprType ∷ ∀ r. Members '[Error TypeError, Reader TypesTable, State Context] r
  ⇒ Expr 'Parsed → Sem r (Maybe TypedExpr)
inferExprType e = case e of
  Unit        → return (Just (TypedExpr Unit UnitTy))
  Var v ()    → inferVar v
  Constr c    → Just . TypedExpr (Constr c) <$> lookupConstrType c
  Lambda{..}  → inferLambda _lamVar _lamVarType _lamExpr
  Case ce as  → inferCase ce as
  App a b     → inferApp a b
  StringLit x → return (Just (TypedExpr (StringLit x) StringTy))
  where
  inferVar ∷ Iden → Sem r (Maybe TypedExpr)
  inferVar v = do
    ctx ← get @Context
    runReader ctx $ do
      VarInfo{..} ← lookupVarInScope v
      return (fmap (TypedExpr (Var v _varBind)) _varType)
  inferApp ∷ Expr 'Parsed → Expr 'Parsed → Sem r (Maybe TypedExpr)
  inferApp a b = do
    ta ← inferExprType a
    case ta of
      Just tyA@(TypedExpr _ (FunTy targ tret)) → do
        tyB ← checkExprType targ b
        return (Just (TypedExpr (App tyA tyB) tret))
      Nothing → return Nothing
      _ → throw ("This does not have a function type: " <> showT a)
  -- | Infers the type of the cased expression by the first pattern.
  -- It tries to infer an alt expression. On success, checks that all alts match that type.
  inferCase ∷ Expr 'Parsed → [CaseAlt 'Parsed] → Sem r (Maybe TypedExpr)
  inferCase cexpr alts = do
    typedCExpr ← checkCExprAndPats cexpr alts
    tyAlts ← firstJustM inferCaseAlt alts
    case tyAlts of
      Nothing → return Nothing
      Just ty → do
        typedAlts ← mapM (checkCaseAltBody ty) alts
        return (Just (TypedExpr (Case typedCExpr typedAlts) ty))
  inferCaseAlt ∷ CaseAlt 'Parsed → Sem r (Maybe Type)
  inferCaseAlt CaseAlt{..} = do
    varTys ← snd <$> inferPatType _altPat
    fmap _exprType . fst <$> withLocalBinds varTys (inferExprType _altExpr)


checkCExprAndPats ∷ Members '[Error TypeError, Reader TypesTable, State Context] r ⇒
  Expr 'Parsed → [CaseAlt 'Parsed] → Sem r TypedExpr
checkCExprAndPats _ [] = error "impossible"
checkCExprAndPats cexpr alts@(CaseAlt{..} : as) = do
    patTy ← fst <$> inferPatType _altPat
    checkAltsExhaustive patTy alts
    mapM_ (checkPatType patTy . (^. altPat)) as
    checkExprType patTy cexpr

inferLambda ∷ ∀ r. Members '[Error TypeError, Reader TypesTable, State Context] r ⇒
   Iden → Maybe Type → Expr 'Parsed → Sem r (Maybe TypedExpr)
inferLambda var varty body = do
  let varInfo = VarInfo varty BindLambdaArg
  (mexprty, varState) ← withLocalBind var varInfo (inferExprType body)
  return $ case (varState, mexprty) of
    (Just argty, Just typedBody@(TypedExpr _ bodyTy)) →
      Just (TypedExpr (Lambda var varty typedBody) (FunTy argty bodyTy))
    (Nothing, _)              → Nothing
    (Just{}, Nothing)         → Nothing

checkAltsExhaustive ∷ Members '[Error TypeError, Reader TypesTable] r ⇒ Type → [CaseAlt 'Parsed] → Sem r ()
checkAltsExhaustive ty alts = case ty of
  UnitTy → return ()
  FunTy{} → error "impossible"
  StringTy{} → throw "Casing a string literal is not allowed."
  TyDataDef ty → do
    tyconstrs ← HS.fromList . map _constructorName <$> lookupType ty
    let altsconstr = HS.fromList . mapMaybe (patConstrIden . _altPat) $ alts
    unless (tyconstrs `HS.isSubsetOf` altsconstr) (throw "Patterns are not exhaustive")
  where
   patConstrIden ∷ Pat → Maybe Iden
   patConstrIden p = case p of
     PUnit    → Nothing
     PApp i _ → Just i
