module Common.Mtl (
  module Control.Monad.Reader
  , module Control.Monad.State
  , module Control.Monad.Writer
  ) where


import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Writer

tell1 ∷ (Applicative f, MonadWriter (f e) m) ⇒ e → m ()
tell1 = tell . pure
 
freezes ∷ MonadState a m ⇒ ReaderT a m b → m b
freezes x = get >>= runReaderT x
