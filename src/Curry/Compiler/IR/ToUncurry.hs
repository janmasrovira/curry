module Curry.Compiler.IR.ToUncurry (compileModule) where


import           Common
import           Common.Polysemy
import           Curry.Compiler.IR.Uncurry.Syntax (TypedExpr (TypedExpr))
import qualified Curry.Compiler.IR.Uncurry.Syntax as U
import qualified Curry.Syntax                     as C
import qualified Curry.Syntax.FreeVars            as C
import           Data.Generics.Uniplate.Data
import qualified Data.HashMap.Strict              as H

data CompilerState = CompilerState {
  _uncurryModule ∷ U.ModuleDef
  , _freshNames  ∷ [U.Iden]
  }
makeLenses ''CompilerState

newCompilerState ∷ CompilerState
newCompilerState = CompilerState {
  _uncurryModule = mempty
  , _freshNames = enumWordsText ['a' ..  'z']
  }

freshName ∷ Members '[State CompilerState] r ⇒ Sem r U.Iden
freshName = do
  f ← gets _freshNames
  let (n : cs) = f
  modify (set freshNames cs)
  return n

freshName' ∷ Members '[State CompilerState] r ⇒ Text → Sem r U.Iden
freshName' hint = (<> ("$" <> hint)) <$> freshName

compileModule ∷ C.ModuleDef 'C.Checked → U.ModuleDef
compileModule m = undefined

structDef ∷ Members '[State CompilerState] r ⇒ U.StructDef → Sem r ()
structDef s = modify (over (uncurryModule . U.structDefs) (++[s]))

funDef ∷ Members '[State CompilerState] r ⇒ U.FunDef → Sem r ()
funDef s = modify (over (uncurryModule . U.funDefs) (++[s]))

exprDef ∷ Members '[State CompilerState] r ⇒ U.ExprDef → Sem r ()
exprDef s = modify (over (uncurryModule . U.exprDefs) (++[s]))

compileModule' ∷ Members '[State CompilerState] r ⇒ C.ModuleDef 'C.Checked → Sem r U.ModuleDef
compileModule' m@C.ModuleDef{..} = undefined

etaExpandConstructors ∷ ∀ r. Members '[State CompilerState] r ⇒ C.ModuleDef 'C.Checked → Sem r (C.ModuleDef 'C.Checked)
etaExpandConstructors = rewriteBiM go
  where
  go ∷ C.TypedExpr → Sem r (Maybe C.TypedExpr)
  go e = case C.viewConstructorApp e of
    Nothing → return Nothing
    Just (constr, args) → case C._exprType e of
      tyconstr@(C.FunTy a b) → do
          varName ← freshName
          let
            var = C.TypedExpr (C.Var varName C.BindLambdaArg) a
            body = C.TypedExpr (C.App e var) b
            lambda = C.TypedExpr (C.Lambda varName (Just a) body) tyconstr
          return (Just lambda)
      _ → return Nothing

compileType ∷ C.Type → U.Type
compileType t = case t of
  C.FunTy f x   → U.FunTy (compileType f) (compileType x)
  C.UnitTy      → U.ConstructorTagTy
  C.StringTy    → U.StringTy
  C.TyDataDef i → U.StructTy i

compileDataDef ∷ Members '[State CompilerState] r ⇒ C.DataDef → Sem r ()
compileDataDef C.DataDef{_rhs = C.SumType{..}, ..} = do
  forM_ (zip [0..] _constructors) (uncurry (compileConstructorDef _name))
  structDef (U.StructDef{_structName = _name, _structRhs = U.Union constructorNames })
  where
  constructorNames = map C._constructorName _constructors

constructorMkName ∷ U.Iden → U.Iden
constructorMkName constructorName = "mk" <> constructorName

callMkConstructor ∷ Members '[Reader C.TypesTable] r ⇒ C.Iden → [U.TypedExpr] → Sem r U.TypedExpr
callMkConstructor constr args = do
  dataTy ← compileType . C.TyDataDef <$> lookupConstructorType constr
  let _funCallFun = constructorMkName constr
      _funCallArgs = args
  return $ U.TypedExpr U.FunCall{..} dataTy

-- | NOTE: Should we treat nullary constructors differently?
compileConstructorDef ∷ ∀ r. Members '[State CompilerState] r ⇒
  U.Iden → Int → C.ConstructorDef → Sem r ()
compileConstructorDef dataName tag C.ConstructorDef{..} = do
  argsNames ← replicateM numArgs freshName
  mkStructDef argsNames
  mkTopFun argsNames
  where
  numArgs = length argsTypes
  argsTypes ∷ [U.Type]
  argsTypes = map compileType _constructorArgs
  mkTopFun ∷ [U.Iden] → Sem r ()
  mkTopFun names = do
    funDef U.FunDef {..}
    where
    _funName = constructorMkName _constructorName
    _funArgs = zipWith U.Arg names argsTypes
    body = U.Struct (U.TypedExpr (U.ConstructorTag tag) U.ConstructorTagTy :
                     [ U.TypedExpr (U.Var name) ty | (U.Arg name ty) ← _funArgs ])
    _funBody = U.TypedExpr body (U.StructTy dataName)
  mkStructDef ∷ [U.Iden] → Sem r ()
  mkStructDef names = do
    _fields ← compileFields names
    structDef (U.StructDef _constructorName U.Record {..})
  compileFields ∷ [U.Iden] → Sem r [U.StructField]
  compileFields names = do
    names ← replicateM numArgs freshName
    return (("tag", U.ConstructorTagTy) : zip names argsTypes)

compileFunDef ∷ Members '[Reader C.TypesTable, State CompilerState] r ⇒ C.FunDef 'C.Checked → Sem r ()
compileFunDef C.FunDef{..} = undefined

lookupRecordFields ∷ Members '[State CompilerState] r ⇒ U.Iden → Sem r [U.StructField]
lookupRecordFields structName = do
  structs ← gets (U._structDefs . _uncurryModule)
  return $ case find ((== structName) . U._structName) structs ^? _Just . U.structRhs of
    Just (U.Record f)  → f
    _ → error "programmer error: lookup non existing record"


compileUnitDef ∷ Members '[State CompilerState] r ⇒ Sem r ()
compileUnitDef = compileDataDef (C.DataDef {
    _name = "()"
    , _rhs = C.SumType {
        _constructors = [C.ConstructorDef "()" []]
        }
    })

lookupConstructor ∷ Members '[Reader C.TypesTable] r ⇒ C.Iden → Sem r C.ConstructorDef
lookupConstructor constr = H.lookupDefault err constr <$> asks C._constructorsDef
  where err = error "programmer error"

lookupConstructorType ∷ Members '[Reader C.TypesTable] r ⇒ C.Iden → Sem r C.Iden
lookupConstructorType constr = H.lookupDefault err constr <$> asks C._constructorsType
  where err = error "programmer error"

constructorPat ∷ Members '[Reader C.TypesTable] r ⇒ C.Iden → Sem r U.Pat
constructorPat constr = do
  consTys ← asks C._constructorsType
  tyMap ← asks C._typesMap
  return $ fromMaybe err $ do
    ty ← H.lookup constr consTys
    constrs ← map C._constructorName <$> H.lookup ty tyMap
    elemIndex constr constrs
  where
  err = error "constructorPat: programmer error"

compileExpr ∷ ∀ r. Members '[Reader C.TypesTable, State CompilerState] r
  ⇒ C.TypedExpr → Sem r U.TypedExpr
compileExpr (C.TypedExpr e ty) = case e of
  C.Unit            → compileUnit
  C.StringLit s     → return (U.TypedExpr (U.StringLit s) U.StringTy)
  C.Case c alts     → compileCase c alts
  C.Var v s         → compileVar v s
  C.Lambda v _ body → compileLambda v body
  C.App f x         → compileApp f x
  C.Constr c        → compileConstructor c
  where
  ty' = compileType ty

  compileUnit ∷ Sem r U.TypedExpr
  compileUnit = return (U.TypedExpr (U.ConstructorTag 0) ty')

  compileApp ∷ C.TypedExpr → C.TypedExpr → Sem r U.TypedExpr
  compileApp f x = do
    f' ← compileExpr f
    x' ← compileExpr x
    return (U.TypedExpr (U.App f' x') ty')

  compileCase ∷ C.TypedExpr → [C.CaseAlt 'C.Checked] → Sem r U.TypedExpr
  compileCase te@(C.TypedExpr e ty) alts = do
    _switchVar ← freshName' "case"
    ce ← compileExpr te
    _switchAlts ← mapM (compileAlt _switchVar) alts
    return $ U.typedLet _switchVar ce (U.TypedExpr U.SwitchInt{..} ty')
    where
    compileAlt ∷ C.Iden → C.CaseAlt 'C.Checked → Sem r U.SwitchAlt
    compileAlt switchVar (C.CaseAlt pat body) = do
      cbody ← compileExpr body
      _altExpr ← case pat of
       C.PUnit            → return cbody
       C.PApp constr vars → do
         fields ← tail <$> lookupRecordFields constr
         let lets = [ (var, U.TypedExpr (U.StructProj constr field switchVar) ty)
                     | (var, (field, ty)) ← zip vars fields ]
         return (U.multiLet lets cbody)
      _altPat ← case pat of
        C.PUnit         → return 0
        C.PApp constr _ → constructorPat constr
      return U.SwitchAlt{..}
    structTypeIden ∷ U.Iden
    structTypeIden = case compileType ty of
      U.StructTy i → i
      _            → error "impossible"

  compileConstructor ∷ C.Iden → Sem r U.TypedExpr
  compileConstructor constr = do
    C.ConstructorDef{..} ← lookupConstructor constr
    case _constructorArgs of
      [] → callMkConstructor constr []
      _  → undefined
    undefined

  compileVar ∷ C.Iden → C.BindSite → Sem r U.TypedExpr
  compileVar = undefined

  compileLambda ∷ C.Iden → C.TypedExpr → Sem r U.TypedExpr
  compileLambda arg t@(C.TypedExpr body bodyty) = do
   structName ← freshName' "Env"
   funName ← freshName' "$fun"
   defineEnvStruct structName
   ubody ← compileExpr t
   defineGlobFun structName funName ubody
   let _funPtr = U.FunctionPointer funName
       _env = map fst freeVars'
   return $ U.TypedExpr U.Closure {..} ty'
   where
   argTy = case ty' of
      U.FunTy a _ → a
      _           → error "impossible"
   defineGlobFun ∷ U.Iden → U.Iden → U.TypedExpr → Sem r ()
   defineGlobFun structName _funName ubody = funDef U.FunDef {
     _funArgs = [U.Arg envArgName (U.StructTy structName),
                 U.Arg arg argTy]
     , _funBody = funBody
     , ..
     }
     where
     envArgName = "$env"
     funBody ∷ U.TypedExpr
     funBody = U.multiLet lets ubody
      where
      lets ∷ [(U.Iden, U.TypedExpr)]
      lets = [ (var, U.TypedExpr (U.StructProj structName var envArgName) ty )
             | (var, ty) ← freeVars' ]
   defineEnvStruct ∷ U.Iden → Sem r ()
   defineEnvStruct _structName = structDef U.StructDef {
     U._structRhs = U.Record freeVars'
     , ..
     }
   freeVars' ∷ [(U.Iden, U.Type)]
   freeVars' = map (second compileType) freeVars
   freeVars ∷ [(C.Iden, C.Type)]
   freeVars = C.freeVars t
   argType = case ty of
    C.FunTy argty _ → argty
    _               → error "impossible"
