module Curry.Compiler.IR.Uncurry.Syntax (
  module Curry.Compiler.IR.Uncurry.Syntax.Base
  , module Curry.Compiler.IR.Uncurry.Syntax
  )
  where

import           Curry.Compiler.IR.Uncurry.Syntax.Base
import           Common

typedLet ∷ Iden → TypedExpr → TypedExpr → TypedExpr
typedLet var def b@(TypedExpr body ty) =
  TypedExpr (Let var def b) ty

multiLet :: [(Iden, TypedExpr)] → TypedExpr → TypedExpr
multiLet = flip $ foldl' (flip (uncurry typedLet))
