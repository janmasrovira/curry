module Common
   (module Common
   , module Lens.Micro.Platform
   , module Control.Monad.Except
   , module Control.Monad.Extra
   , module Control.Monad.Identity
   , module Data.Char
   , module Data.Either.Extra
   , module Data.Function
   , module Data.List.Extra
   , module Data.Maybe
   , module Data.String
   , module Data.Text.Encoding
   , module Data.Tuple.Extra
   , module Data.Void
   , module System.Directory
   , module System.FilePath
   , module Data.Singletons
   , Data
   , Text
   , pack
   , unpack
   , strip
   , HashMap
   , ByteString
   , NonEmpty(..)
   , HashSet
   , IsString(..)
   , Alternative(..)
   )
where

import           Control.Applicative
import Lens.Micro.Platform hiding (both)
import           Control.Monad.Except
import           Control.Monad.Extra
import           Control.Monad.Identity
import           Data.ByteString.Lazy   (ByteString)
import qualified Data.ByteString.Lazy   as B
import           Data.Char
import           Data.Data
import           Data.Either.Extra
import           Data.Function
import           Data.HashMap.Strict    (HashMap)
import qualified Data.HashMap.Strict    as HashMap
import           Data.HashSet           (HashSet)
import qualified Data.HashSet           as HS
import           Data.Hashable          (Hashable)
import           Data.List              (foldl')
import           Data.List.Extra
import           Data.List.NonEmpty     (NonEmpty (..))
import           Data.Maybe
import           Data.Singletons
import           Data.String
import           Data.Text              (Text, pack, strip, unpack)
import qualified Data.Text              as Text
import           Data.Text.Encoding
import qualified Data.Text.IO           as Text
import           Data.Tuple.Extra
import           Data.Void
import           System.Directory
import           System.FilePath

(.||.) ∷ (a → Bool) → (a → Bool) → a → Bool
(f .||. g) x = f x || g x


pipeApp ∷ [a → a] → a → a
pipeApp ts a = foldl' (flip ($)) a ts

-- | Lowers the first letter of a 'Text'.
uncapitalize ∷ Text → Text
uncapitalize (Text.uncons → Nothing)      = ""
uncapitalize (Text.uncons → Just (l, ls)) = Text.cons (toLower l) ls

fromRightIO ∷ Either Text a → IO a
fromRightIO e = case e of
  Right r  → return r
  Left err → fail (show err)

textSpaces ∷ Int → Text
textSpaces n = Text.replicate n (Text.singleton ' ')

errorT ∷ Text → a
errorT = error . unpack

showT ∷ Show s ⇒ s → Text
showT = pack . show

fromText ∷ IsString s ⇒ Text → s
fromText = fromString . unpack

insertIfNotThere ∷ (Eq k, Hashable k) ⇒ k → a → HashMap k a → Maybe (HashMap k a)
insertIfNotThere k val m = case HashMap.lookup k m of
  Just _  → Nothing
  Nothing → Just (HashMap.insert k val m)

allDiferent ∷ (Eq k, Hashable k) ⇒ [k] → Bool
allDiferent l = length l == HS.size (HS.fromList l)

enumWords ∷ [a] → [[a]]
enumWords a = go [[]]
  where go lshorter = let t = [ x:shorter | x ← a, shorter ← lshorter ]
                     in t ++ go t

enumWordsText ∷ [Char] → [Text]
enumWordsText = map pack . enumWords
