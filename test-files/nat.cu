data Nat =
 | Zero
 | Suc Nat ;

data Bool =
 | True
 | False;

def add : Nat → Nat → Nat = λ x → (λ y →
  case x of
 | Zero → y
 | Suc k → Suc (add k y));

def eq : Nat → Nat → Bool =
  λ n → λ m → case n of
  | Zero → (case m of
    | Zero → True
    | Suc b → False)
  | Suc n → (case m of
    | Zero → False
    | Suc b → eq n b);

def main : () = ();