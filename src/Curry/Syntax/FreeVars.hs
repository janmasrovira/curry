module Curry.Syntax.FreeVars (freeVars) where

import           Common
import           Curry.Syntax.Base
import           Curry.Syntax.Extra
import qualified Data.HashMap.Strict as HS
import           Data.Hashable

type FreeVars = HashMap Iden Type

freeVars ∷ TypedExpr → [(Iden, Type)]
freeVars = HS.toList . go
  where
  go ∷ TypedExpr → FreeVars
  go TypedExpr{..} = case _expr of
   Unit        → mempty
   Var v _     → HS.singleton v _exprType
   App f x     → go f <> go x
   Constr{}    → mempty
   StringLit{} → mempty
   Lambda{..}  → HS.delete _lamVar (go _lamExpr)
   Case e alts → go e <> mconcatMap freeVarsAlt alts
   where
   deletes ∷ FreeVars → [Iden] → FreeVars
   deletes = foldl' (flip HS.delete)
   freeVarsAlt ∷ CaseAlt 'Checked → FreeVars
   freeVarsAlt CaseAlt{..} = deletes (go _altExpr) (patVars _altPat)
   patVars ∷ Pat → [Iden]
   patVars p = case p of
     PUnit       → mempty
     PApp _ args → args
