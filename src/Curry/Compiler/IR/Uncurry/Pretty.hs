module Curry.Compiler.IR.Uncurry.Pretty where

import Common
import Curry.Compiler.IR.Uncurry.Syntax
import Prettyprinter

data Ann =
  Keyword
  | IdenDef

newtype Config = Config {
  _indent :: Int
  }

config :: Config
config = Config {
  _indent = 2
  }
