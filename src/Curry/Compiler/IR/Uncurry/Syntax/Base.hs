module Curry.Compiler.IR.Uncurry.Syntax.Base where

import           Common

type Iden = Text
type StructIden = Iden
type FieldName = Iden

data Type
  = -- | A user defined struct.
    StructTy Iden
  | -- | The unit type.
    ConstructorTagTy
  | -- | String type. Used for literal strings.
    StringTy
  | -- | Function type
    FunTy Type Type
  deriving (Show, Eq, Data)

makeLenses ''Type

data TopFunType = FunType [Type] Type

type ClosureIden = Int
type ConstructorTag = Int

newtype FunctionPointer = FunctionPointer
  { _ptrFunName ∷ Iden
  }
  deriving (Show)

type StructField = (Iden, Type)

data StructDef = StructDef
  { _structName ∷ Iden,
    _structRhs  ∷ StructRhs
  }
  deriving (Show)

data StructRhs
  = Record
      { _fields ∷ [StructField]
      }
  | Union
      { _structsNames ∷ [Iden]
      }
  deriving (Show, Data)

makeLenses ''StructRhs
makeLenses ''StructDef

type Pat = Int

data TypedExpr = TypedExpr {
  _expr       ∷ Expr
  , _exprType ∷ Type
  }
  deriving (Show)


data SwitchAlt = SwitchAlt
  { _altPat  ∷ Pat,
    _altExpr ∷ TypedExpr
  }
  deriving (Show)

data Arg = Arg
  { _argName ∷ Iden,
    _argType ∷ Type
  }
  deriving (Show)

data Expr
  = 
  SwitchInt
      { _switchVar ∷ Iden,
        _switchAlts ∷ [SwitchAlt]
      }
  | App TypedExpr TypedExpr
  | Var
      { _varIden ∷ Iden
      }
  | FunCall {
      _funCallFun ∷ Iden,
      _funCallArgs ∷ [TypedExpr]
      }
  | StringLit Text
  | ConstructorTag Int
  | Struct {
      _structArgs ∷ [TypedExpr]
      }
  | Let {
      _letVar ∷ Iden,
      _letExpr ∷ TypedExpr,
      _letBody ∷ TypedExpr
      }
  | Closure
      { _funPtr ∷ FunctionPointer,
        _env    ∷ [Iden]
      }
  | StructProj {
      _projStruct    ∷ StructIden,
      _projFieldName ∷ FieldName,
      _projVar       ∷ Iden
      }
  deriving (Show)

makeLenses ''SwitchAlt
makeLenses ''Expr

data FunDef = FunDef
  { _funName ∷ Iden,
    _funArgs ∷ [Arg],
    _funBody ∷ TypedExpr
  }
  deriving (Show)

makeLenses ''FunDef

data ExprDef = ExprDef
  { _exprName ∷ Iden,
    _exprBody ∷ TypedExpr
  }
  deriving (Show)

makeLenses ''ExprDef

data ModuleDef = ModuleDef
  { _structDefs ∷ [StructDef],
    _funDefs    ∷ [FunDef],
    _exprDefs   ∷ [ExprDef]
  }
  deriving (Show)

makeLenses ''ModuleDef

instance Semigroup ModuleDef where
  a <> b = ModuleDef {
    _structDefs = _structDefs a <> _structDefs b
    , _funDefs = _funDefs a <> _funDefs b
    , _exprDefs = _exprDefs a <> _exprDefs b
    }

instance Monoid ModuleDef where
  mempty = ModuleDef {
    _structDefs = mempty
    , _funDefs = mempty
    , _exprDefs = mempty
    }
