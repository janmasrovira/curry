module Curry.Frontend.Haskelly.Parser.Base where

import           Common
import           Text.Megaparsec            as M
import  qualified Data.Text as Text
import           Text.Megaparsec.Char       (space1)
import qualified Text.Megaparsec.Char.Lexer as L

type OperatorSym = Text

space ∷ MonadParsec e Text m ⇒ m ()
space = L.space space1 lineComment block
  where
    lineComment = L.skipLineComment "--" <|> L.skipLineComment "//"
    block = L.skipBlockComment "{-" "-}"

lexeme ∷ MonadParsec e Text m ⇒ m a → m a
lexeme = L.lexeme space

symbol ∷ MonadParsec e Text m ⇒ Text → m ()
symbol = void . L.symbol space

symbol' ∷ MonadParsec e Text m ⇒ Text → m ()
symbol' = void . L.symbol' space

decimal ∷ (MonadParsec e Text m, Num n) ⇒ m n
decimal = lexeme L.decimal

constructorIden ∷ MonadParsec e Text m ⇒ m Text
constructorIden = do
  notFollowedBy (choice allKeyConstrs)
  lexeme $ do
    h <- M.satisfy isUpper
    Text.cons h <$> identifierTail

operatorSym ∷ MonadParsec e Text m ⇒ m OperatorSym
operatorSym = lexeme $
  M.takeWhile1P Nothing (isAlphaNum .||. isSymbol .||. isPunctuation)

identifierTail ∷ MonadParsec e Text m ⇒ m Text
identifierTail = M.takeWhileP Nothing (isAlphaNum .||. (`elem`("_'" ∷ String)))

identifier ∷ MonadParsec e Text m ⇒ m Text
identifier = do
  notFollowedBy (choice allKeywords)
  lexeme $ do
    h <- M.satisfy isLower
    Text.cons h <$> identifierTail

allKeywords ∷ MonadParsec e Text m ⇒ [m ()]
allKeywords =
  [
    kwLet
  , kwIn
  , kwCase
  , kwOf
  , kwData
  , kwDef
  ]

allKeyConstrs ∷ MonadParsec e Text m ⇒ [m ()]
allKeyConstrs =
  [
    kcString
  ]

parens ∷ MonadParsec e Text m ⇒ m a → m a
parens = between (symbol "(") (symbol ")")

kwLet ∷ MonadParsec e Text m ⇒ m ()
kwLet = symbol' "let"

kwData ∷ MonadParsec e Text m ⇒ m ()
kwData = symbol' "data"

kwDef ∷ MonadParsec e Text m ⇒ m ()
kwDef = symbol' "def"

semi ∷ MonadParsec e Text m ⇒ m ()
semi = symbol ";"

ksPipe ∷ MonadParsec e Text m ⇒ m ()
ksPipe = symbol "|"

ksEquals ∷ MonadParsec e Text m ⇒ m ()
ksEquals = symbol "="

ksColon ∷ MonadParsec e Text m ⇒ m ()
ksColon = symbol ":"

ksArrowR ∷ MonadParsec e Text m ⇒ m ()
ksArrowR = symbol "→" <|> symbol "->"

ksLambda ∷ MonadParsec e Text m ⇒ m ()
ksLambda = symbol "λ" <|> symbol "\\"

kwCase ∷ MonadParsec e Text m ⇒ m ()
kwCase = symbol' "case"

kwOf ∷ MonadParsec e Text m ⇒ m ()
kwOf = symbol' "of"

kwIn ∷ MonadParsec e Text m ⇒ m ()
kwIn = symbol' "in"

kcString ∷ MonadParsec e Text m ⇒ m ()
kcString = symbol' "String"
