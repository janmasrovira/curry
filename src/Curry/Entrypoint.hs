module Curry.Entrypoint where

import           Common
import           Curry.Codegen
import           Curry.Frontend.Haskelly.Parser
import           Curry.Frontend.Haskelly.Parser.Preparser (parseOperators)
import           Curry.Typechecker
import           Data.Text.IO                             as T

appMain ∷ IO ()
appMain = do
  s <- T.readFile "test-files/bool.cu"
  case parseModule s of
    Left e → T.putStr $ "Error: " <> e
    Right o → do
      T.putStrLn $ showT o
      T.putStrLn "\n------------\n"
      checked ← fromRightIO (runCheckModule o)
      T.putStrLn "Ok"
      T.putStrLn "\n------------\n"
      T.putStrLn (genModuleText checked)


