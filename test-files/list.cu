data Nat =
 | Zero
 | Suc Nat ;

data Bool =
 | True
 | False;

data List =
   | Empty
   | Cons Nat List;


def length : List → Nat = λ l → case l of
    | Empty → Zero
    | Cons a as → Suc (length as);

def cons = λ x → λ l → Cons x l;

def shadow : Bool → Nat → Nat = λ n → λ n → n;

def main : () = ();