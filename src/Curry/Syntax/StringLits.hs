module Curry.Syntax.StringLits where

import           Common
import           Curry.Syntax.Base
import           Curry.Syntax.Extra
import qualified Data.HashSet      as HS

go ∷ Expr 'Parsed → HashSet Text
go e = case e of
  Unit        → mempty
  Var{}       → mempty
  App f x     → go f <> go x
  Constr{}    → mempty
  StringLit i → HS.singleton i
  Lambda{..}  → go _lamExpr
  Case e alts → go e <> mconcatMap go (map _altExpr alts)


moduleStringLits ∷ ∀ (s ∷ Stage). SingI s ⇒ ModuleDef s → HashSet Text
moduleStringLits = mconcatMap (go . _funExpr) . _funDefs . uncheckModuleDef
