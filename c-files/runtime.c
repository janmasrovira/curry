#include <stdio.h>
#include <stdlib.h>

typedef unsigned char byte;

void printInt(int x) { printf("%d", x); }

void printString(char *str) { printf("%s", str); }

int readInt() {
  int r;
  scanf("%d", &r);
  return r;
}
