module Curry.Syntax.Extra where

import           Curry.Syntax.Base
import           Data.Singletons

uncheckTypedExpr ∷ TypedExpr → Expr 'Parsed
uncheckTypedExpr = uncheckExpr . _expr

-- | Returns the corresponding untyped expression.
uncheckExpr ∷ ∀ (a ∷ Stage). SingI a ⇒ Expr a → Expr 'Parsed
uncheckExpr = aux sing
  where
  aux ∷ SStage a → Expr a → Expr 'Parsed
  aux SParsed  e = e
  aux SChecked e  = case e of
    Var i s      → Var i ()
    Constr i     → Constr i
    Unit         → Unit
    StringLit t  → StringLit t
    App f x      → App (uncheckTypedExpr f) (uncheckTypedExpr x)
    Lambda v t e → Lambda v t (uncheckTypedExpr e)
    Case e alts  → Case (uncheckTypedExpr e) (map ua alts)
    where
    ua ∷ CaseAlt 'Checked → CaseAlt 'Parsed
    ua (CaseAlt p e) = CaseAlt p (uncheckTypedExpr e)

uncheckFunDef ∷ ∀ (a ∷ Stage). SingI a ⇒ FunDef a → FunDef 'Parsed
uncheckFunDef = aux sing
  where
  aux ∷ SStage a → FunDef a → FunDef 'Parsed
  aux SParsed d           = d
  aux SChecked FunDef{..} = FunDef{_funExpr = uncheckTypedExpr _funExpr, ..}

uncheckModuleDef ∷ ∀ (a ∷ Stage). SingI a ⇒ ModuleDef a → ModuleDef 'Parsed
uncheckModuleDef = aux sing
  where
  aux ∷ SStage a → ModuleDef a → ModuleDef 'Parsed
  aux SParsed d = d
  aux SChecked ModuleDef{..} = ModuleDef{_funDefs = map uncheckFunDef _funDefs, _extra = (), ..}
