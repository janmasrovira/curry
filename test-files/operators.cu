infixl + 12;
def + : () → () → () = λ x → λ y → ();

def test = () + () + () + ();

infixr ∘ 16;
def ∘ : () → () → () = λ x → λ y → ();

def test2 = () + () ∘ () + ();


def main : () = ();