module Curry.Codegen where

import           Common
import           Common.Mtl
import           Curry.Syntax
import qualified Data.HashMap.Strict     as H
import qualified Data.HashSet            as HS
import qualified Data.Text.Lazy          as T
import qualified LLVM.AST                as IR
import qualified LLVM.AST.Constant       as IR
import qualified LLVM.AST.Type           as IR
import qualified LLVM.AST.Typed          as IR
import           LLVM.IRBuilder          (MonadIRBuilder, MonadModuleBuilder)
import qualified LLVM.IRBuilder          as IR
import qualified LLVM.IRBuilder.Constant as IR
import           LLVM.Pretty

data GenState = GenState {
  _constructorsId    ∷ HashMap Iden Int
  , _stringLitsTable ∷ HashMap Text IR.Constant
  , _varTable        ∷ HashMap Iden (IR.Type, Int)
  }
makeLenses ''GenState

data GlobalEnv = GlobalEnv {
  }

iniState ∷ GenState
iniState = GenState {
  _constructorsId = mempty
  , _stringLitsTable = mempty
  , _varTable = mempty
}

genModuleText ∷ ModuleDef 'Checked → Text
genModuleText = T.toStrict . ppllvm . genModule

genModule ∷ ModuleDef 'Checked → IR.Module
genModule = flip evalState iniState . IR.buildModuleT "test" . genModule'

genModule' ∷ (MonadState GenState m, MonadModuleBuilder m) ⇒ ModuleDef 'Checked → m ()
genModule' m@ModuleDef{..} = do
  defineStrings m
  mapM_ genDataDef _typeDefs
  mapM_ genFunDef _funDefs

-- | Defines all string literals found in the given module as global variables.
-- It stores the reference in the 'GenState'.
defineStrings ∷ ∀ m. (MonadState GenState m, MonadModuleBuilder m) ⇒ ModuleDef 'Checked → m ()
defineStrings m = mapM_ defStr (zip [0..] allStrings)
  where
  allStrings ∷ [Text]
  allStrings = HS.toList (moduleStringLits m)
  defStr ∷ (Int, Text) → m ()
  defStr (num, t) = do
    let name = fromText ("glob_str_" <> showT num)
    c ← IR.globalStringPtr (unpack t) name
    modifying stringLitsTable (H.insert t (IR.GlobalReference (IR.typeOf c) name))


genDataDef ∷ (MonadState GenState m, MonadModuleBuilder m) ⇒ DataDef → m ()
genDataDef DataDef{_rhs = SumType{..}, ..} = do
  genConstructorsDef _constructors

genConstructorsDef ∷ (MonadState GenState m, MonadModuleBuilder m) ⇒ [ConstructorDef] → m ()
genConstructorsDef = mapM_ (uncurry genConstructorDef) . zip [0..]

-- consumeCounter ∷ MonadState GenState m ⇒ m Int
-- consumeCounter = do
--   k ← gets _counter
--   modifying counter (+1)
--   return k

-- | Here we define the type of the fully applied constructor.
genConstructorDef ∷ (MonadState GenState m, MonadModuleBuilder m)
  ⇒ Int → ConstructorDef → m ()
genConstructorDef i ConstructorDef{..} = do
  modifying constructorsId (H.insert _constructorName i)
  IR.emitDefn $
    IR.TypeDefinition (fromText _constructorName)
    (Just IR.StructureType {
             isPacked = False
             , elementTypes = [IR.i16]
             })

genFunDef ∷ ∀ m. (MonadState GenState m, MonadModuleBuilder m)
  ⇒ FunDef 'Checked → m ()
genFunDef FunDef{..} = case ty of
  _ → return ()
  where
  ty = _exprType _funExpr

genExpr ∷ ∀ m. (MonadState GenState m, MonadModuleBuilder m, MonadIRBuilder m)
  ⇒ TypedExpr → m IR.Operand
genExpr (TypedExpr e ty) =
  case e of
    Unit         → return $ IR.bit 1
    Case ce alts → undefined
    Var iden s   → undefined
    App f x      → undefined
    Constr f     → undefined
    StringLit x  → gString x
    Lambda{..}   → undefined
  where
  gString ∷ Text → m IR.Operand
  gString t = do
    globRef ← gets (fromMaybe (perror $ "did not find string " <> t) . H.lookup t . _stringLitsTable)
    IR.gep (IR.ConstantOperand globRef) [IR.int32 0, IR.int32 0]

perror ∷ Text → a
perror e = error ("Programmer error: " <> unpack e)
