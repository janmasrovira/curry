{-# LANGUAGE QuantifiedConstraints #-}
module Curry.Frontend.Haskelly.Parser where

import           Common
import           Common.Mtl
import           Control.Applicative                      (Alternative (many, some),
                                                           empty, (<|>))
import           Control.Applicative.Combinators
import           Control.Monad.Combinators.Expr
import qualified Control.Monad.Trans.Reader               as T
import           Curry.Frontend.Haskelly.Parser.Base
import           Curry.Frontend.Haskelly.Parser.Preparser
import           Curry.Syntax.Base
import           Text.Megaparsec                          (MonadParsec)
import qualified Text.Megaparsec                          as M
import           Text.Megaparsec.Char                     (space1)
import qualified Text.Megaparsec.Char                     as L
import qualified Text.Megaparsec.Char.Lexer               as L

type PExpr = Expr Parsed
type PModuleDef = ModuleDef Parsed
type PCaseAlt = CaseAlt Parsed
type PFunDef = FunDef Parsed

pType ∷ MonadParsec e Text m ⇒ m Type
pType = expr
  where
  expr = makeExprParser termTy table
  table = [[InfixR (FunTy <$ ksArrowR)]]
  termTy =
     unitTy
    <|> parens expr
    <|> StringTy <$ kcString
    <|> TyDataDef <$> constructorIden

unitTy ∷ MonadParsec e Text m ⇒ m Type
unitTy = UnitTy <$ symbol "()"

var ∷ MonadParsec e Text m ⇒ m PExpr
var = do
  _varIden ← identifier
  let _bindSite = ()
  return Var {..}

constr ∷ MonadParsec e Text m ⇒ m PExpr
constr = Constr <$> constructorIden

pApp ∷ MonadParsec e Text m ⇒ m Pat
pApp = PApp <$> constructorIden <*> many identifier

pat ∷ MonadParsec e Text m ⇒ m Pat
pat =
  PUnit <$ symbol "()"
  <|> pApp

pAlt ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PCaseAlt
pAlt = do
  pexpr <- T.ask
  ksPipe
  p <- pat
  ksArrowR
  CaseAlt p <$> lift pexpr

pCase ∷ (MonadParsec e Text m) ⇒ ReaderT (m PExpr) m PExpr
pCase = do
  kwCase
  p <- T.ask >>= lift
  kwOf
  alts <- some pAlt
  return $ Case p alts

-- TODO: The parser should not be responsible of desugaring.
pLet ∷ (MonadParsec e Text m) ⇒ ReaderT (m PExpr) m PExpr
pLet = do
  kwLet
  v <- identifier
  ksEquals
  def <- T.ask >>= lift
  kwIn
  e <- T.ask >>= lift
  return $ App (Lambda v Nothing e) def

lambda ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PExpr
lambda = do
  ksLambda
  (_lamVar, _lamVarType) <- lambdaArg
  ksArrowR
  _lamExpr <- T.ask >>= lift
  return $ Lambda{..}

stringLit ∷ MonadParsec e Text m ⇒ m PExpr
stringLit = lexeme $ do
  symbol "\""
  StringLit . pack <$> manyTill L.charLiteral (symbol "\"")

lambdaArg ∷ MonadParsec e Text m ⇒ m (Iden, Maybe Type)
lambdaArg = do
  parens (do
             v <- identifier
             ksColon
             t <- pType
             return (v, Just t))
  <|> ((, Nothing) <$> identifier)


parensExpr ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PExpr
parensExpr = parens (T.ask >>= lift)

unitTerm ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PExpr
unitTerm = Unit <$ symbol "()"

term ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PExpr
term =
  unitTerm
  <|> parensExpr
  <|> lambda
  <|> pCase
  <|> pLet
  <|> var
  <|> constr
  <|> stringLit

anyDef ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m (Maybe PFunDef)
anyDef =
  (Nothing <$ lift pOperatorDef)
  <|> (Just <$> funDef)

funDef ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PFunDef
funDef = M.label "function definition" $ do
  kwDef
  _funName <- M.try identifier <|> operatorSym
  _funType <- optional $ ksColon >> pType
  ksEquals
  pexpr <- T.ask
  _funExpr <- lift pexpr
  return FunDef{..}

dataDef ∷ MonadParsec e Text m ⇒ m DataDef
dataDef = M.label "data definition" $ do
  kwData
  _name <- constructorIden
  ksEquals
  _rhs <- dataDefRhs
  return DataDef{..}

constructorArgType ∷ MonadParsec e Text m ⇒ m Type
constructorArgType = do
  unitTy
  <|> TyDataDef <$> constructorIden
  <|> parens pType

constructorDef ∷ MonadParsec e Text m ⇒ m ConstructorDef
constructorDef = do
  ksPipe
  _constructorName <- constructorIden
  _constructorArgs <- many constructorArgType
  return ConstructorDef {..}

dataDefRhs ∷ MonadParsec e Text m ⇒ m DataDefRhs
dataDefRhs = SumType <$> some constructorDef

moduleDef' ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PModuleDef
moduleDef' = do
  _typeDefs <- sepEndBy dataDef semi
  _funDefs <- catMaybes <$> sepEndBy anyDef semi
  let _extra = ()
  return $ ModuleDef{..}

moduleDef ∷ MonadParsec e Text m ⇒ ReaderT (m PExpr) m PModuleDef
moduleDef = space >> moduleDef' <* M.eof

parseModule ∷ Text → Either Text PModuleDef
parseModule input = do
  ops <- mapLeft (pack . M.errorBundlePretty) $ M.parse allOperatorDefs "" input
  let pexpr = mkExprParser ops ∷ M.Parsec Void Text PExpr
      pmod = runReaderT moduleDef pexpr
  mapLeft (pack . M.errorBundlePretty) $ M.parse pmod "" input

mkExprParser ∷ [OperatorDef] → (∀ e m . MonadParsec e Text m ⇒ m PExpr)
mkExprParser ops = pexpr
  where
    pexpr = makeExprParser termAux table
    table = mkOperatorsTable ops
    termAux = runReaderT term pexpr

mkOperatorsTable ∷ ∀ m e. MonadParsec e Text m ⇒ [OperatorDef] → [[Operator m PExpr]]
mkOperatorsTable = addApp . map (map toOperator) . groupSortOn _prec
  where
  addApp ∷ [[Operator m PExpr]] → [[Operator m PExpr]]
  -- TODO not followed by operator
  addApp = cons [InfixL (App <$ space)]
  toOperator ∷ OperatorDef → Operator m PExpr
  toOperator OperatorDef{..} = case _arity of
    Unary p  → prePost p $ auxUn _opSymbol (App (Var _opSymbol ()))
    Binary a → sideAssoc a $ auxBin _opSymbol (App . App (Var _opSymbol ()))
  auxBin ∷ Text → (PExpr → PExpr → PExpr) → m (PExpr → PExpr → PExpr)
  auxBin name f = f <$ symbol name
  auxUn ∷ Text → (PExpr → PExpr) → m (PExpr → PExpr)
  auxUn name f = f <$ symbol name
  prePost ∷ UnaryAssoc → m (PExpr → PExpr) → Operator m PExpr
  prePost a = case a of
    PrefixOp  → Prefix
    PostfixOp → Postfix
  sideAssoc ∷ BinaryAssoc → m (PExpr → PExpr → PExpr) → Operator m PExpr
  sideAssoc a = case a of
    None       → InfixN
    LeftAssoc  → InfixL
    RightAssoc → InfixR
