#include <stdint.h>
int tmp = 123+3;

struct Cons {
  double head;
  struct List* tail;
};

struct Cons2 {
  int nop;
  int haha;
  int haha2;
  struct List *tail;
};

struct Nil {
  int64_t something;
};

struct List {
  enum { Cons, Nil, Cons2 } tag;
  union Kappa {
    struct Cons cons;
    struct Nil nil;
    struct Cons2 cons2;
  } data;
};

double sum(const struct List* list) {
  switch (list->tag) {
  case Nil:
    return list->data.nil.something;
  case Cons:
    return sum(list->data.cons.tail) + list->data.cons.head;
  case Cons2:
    return 0;
  }
}

int main() {
  /* wrapper(); */
  struct List l1 = {Nil, {.nil = 123}};
  struct List l2 = {Cons, {.cons = 1.2, &l1 }};
  struct List l3 = {Cons2, {.cons2 = 3, 33, 123, &l1}};
  return 0;
}
