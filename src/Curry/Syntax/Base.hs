module Curry.Syntax.Base where

import           Common
import           Data.Data
import           Data.Generics.Uniplate.Data
import qualified Data.Kind                   as GHC
import           Data.Singletons.TH

data Stage =
  Parsed
  | Checked
  deriving (Show)
genSingletons [''Stage]

type Iden = Text

data Type =
  -- | A user defined type.
  TyDataDef Iden
  -- | The type of functions: a → b.
  | FunTy Type Type
  -- | The unit type.
  | UnitTy
  -- | String type. Used for literal strings.
  | StringTy
 deriving (Show, Eq, Data, Typeable)
makeLenses ''Type

data ConstructorDef =
  ConstructorDef {
  _constructorName   ∷ Iden
  , _constructorArgs ∷ [Type]
  }
 deriving (Show, Data, Typeable)
makeLenses ''ConstructorDef

-- | The right hand side of a type definition.
newtype DataDefRhs =
  -- | A sum type definition.
  SumType {
  -- | The constructors.
      _constructors ∷ [ConstructorDef]
      }
 deriving (Show, Data, Typeable)
makeLenses ''DataDefRhs

data DataDef = DataDef {
  _name  ∷ Iden
  , _rhs ∷ DataDefRhs
  }
 deriving (Show, Data, Typeable)
makeLenses ''DataDef

data Pat =
  PApp Iden [Iden]
  | PUnit
  deriving (Show, Data, Typeable)

data CaseAlt (stage ∷ Stage) = CaseAlt {
  _altPat    ∷ Pat
  , _altExpr ∷ ExprTy stage
 }
  deriving (Typeable)
deriving instance Show (ExprTy stage) ⇒ Show (CaseAlt stage)
deriving instance (Typeable stage, Data (ExprTy stage)) ⇒ Data (CaseAlt stage)

-- | ExprTy determines whether we have untyped (parsing) or typed expressions
-- (after typechecking). Instances must be declared afterwards due to cyclic dependency.
type family ExprTy (a ∷ Stage) ∷ GHC.Type

type family VarBindSite (a ∷ Stage) ∷ GHC.Type where
  VarBindSite 'Parsed = ()
  VarBindSite 'Checked = BindSite

type family ExtraConTy (a ∷ Stage) ∷ GHC.Type where
  ExtraConTy 'Parsed = Void
  ExtraConTy 'Checked = Int


data BindSite =
  BindTopFun
  | BindLambdaArg
  | BindPattern {
      _bindConstructorName ∷ Iden
      , _argumentIx        ∷ Int
      }
  deriving (Show, Eq, Data, Typeable)

data Expr (stage ∷ Stage) =
  Unit
  | Lambda {
      _lamVar       ∷ Iden
      , _lamVarType ∷ Maybe Type
      , _lamExpr    ∷ ExprTy stage
      }
  | Case {
      _caseExpr   ∷ ExprTy stage
      , _caseAlts ∷ [CaseAlt stage]
      }
  | App (ExprTy stage) (ExprTy stage)
  | Constr {
      _constrIden ∷ Iden
           }
  | Var {
      _varIden    ∷ Iden
      , _bindSite ∷ VarBindSite stage
      }
  | StringLit Text
deriving instance (Show (VarBindSite stage), Show (ExprTy stage)) ⇒ Show (Expr stage)
deriving instance (Typeable stage, Data (VarBindSite stage), Data (ExprTy stage)) ⇒ Data (Expr stage)

makeLenses ''CaseAlt
makeLenses ''Expr

type instance ExprTy 'Parsed = Expr 'Parsed
type instance ExprTy 'Checked = TypedExpr

data TypedExpr = TypedExpr {
  _expr       ∷ Expr 'Checked
  , _exprType ∷ Type
  }
  deriving (Typeable, Data)

data FunDef (stage ∷ Stage) = FunDef {
  _funName   ∷ Iden
  , _funType ∷ Maybe Type
  , _funExpr ∷ ExprTy stage
  }
deriving instance Show (ExprTy stage) ⇒ Show (FunDef stage)
deriving instance (Typeable stage, Data (VarBindSite stage), Data (ExprTy stage)) ⇒ Data (FunDef stage)
makeLenses ''FunDef

type family ModuleDefExtra (a ∷ Stage) ∷ GHC.Type
type instance ModuleDefExtra 'Parsed = ()
type instance ModuleDefExtra 'Checked = TypesTable

data TypesTable = TypesTable {
  -- | constructors → types
  _constructorsType  ∷ HashMap Iden Iden
  -- | type → constructors
  , _typesMap        ∷ HashMap Iden [ConstructorDef]
  -- | constructor → its definition
  , _constructorsDef ∷ HashMap Iden ConstructorDef
  }
  deriving (Show, Data, Typeable)
makeLenses ''TypesTable

data ModuleDef (stage ∷ Stage) = ModuleDef {
  _typeDefs  ∷ [DataDef]
  , _funDefs ∷ [FunDef stage]
  , _extra   ∷ ModuleDefExtra stage
 }
deriving instance (Show (ExprTy stage), Show (ModuleDefExtra stage)) ⇒ Show (ModuleDef stage)
deriving instance (Typeable stage, Data (ModuleDefExtra stage), Data (VarBindSite stage), Data (ExprTy stage)) ⇒ Data (ModuleDef stage)
makeLenses ''ModuleDef

instance Semigroup (ModuleDefExtra stage) ⇒ Semigroup (ModuleDef stage) where
  a <> b = ModuleDef {
    _typeDefs = _typeDefs a <> _typeDefs b
    , _funDefs = _funDefs a <> _funDefs b
    , _extra = _extra a <> _extra b
    }

instance Monoid (ModuleDefExtra stage) ⇒ Monoid (ModuleDef stage) where
  mempty = ModuleDef {
    _typeDefs = mempty
    , _funDefs = mempty
    , _extra = mempty
    }
