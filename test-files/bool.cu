data Bool =
 | True
 | False;

def not = λ b → case b of
  | False → True
  | True → False;

def or = λ a → λ b →
   case a of
  | True → True
  | False → b;

def printBool = λ b → case b of
  | False → "false"
  | True → "true";

def main = ()