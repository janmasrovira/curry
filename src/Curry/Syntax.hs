module Curry.Syntax (
  module Curry.Syntax.Base
  , module Curry.Syntax
  , module Curry.Syntax.StringLits
  , module Curry.Syntax.FreeVars
  ) where

import           Curry.Syntax.Base
import           Curry.Syntax.FreeVars
import           Curry.Syntax.StringLits

funTyFromSig ∷ [Type] → Type → Type
funTyFromSig = flip (foldr FunTy)

typedUnit ∷ TypedExpr
typedUnit = TypedExpr Unit UnitTy

viewApp ∷ TypedExpr → TypedExpr → (TypedExpr, [TypedExpr])
viewApp f x = case go f of
                (fun, args) → (fun, reverse (x : args))
  where
  go ∷ TypedExpr → (TypedExpr, [TypedExpr])
  go expr@(TypedExpr e ty) = case e of
    Unit        → (expr, [])
    Var{}       → (expr, [])
    Constr{}    → (expr, [])
    StringLit{} → (expr, [])
    Lambda{}    → (expr, [])
    Case{}      → (expr, [])
    App f x     → case go f of
      (fun, args) → (fun, x : args)

viewConstructorApp ∷ TypedExpr → Maybe (Iden, [TypedExpr])
viewConstructorApp (TypedExpr e ty) = case e of
  Constr cons → Just (cons, [])
  App f x → case viewApp f x of
    (TypedExpr (Constr cons) _, args) → Just (cons, args)
    _ → Nothing
  _ → Nothing
