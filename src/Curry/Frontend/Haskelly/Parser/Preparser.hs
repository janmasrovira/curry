module Curry.Frontend.Haskelly.Parser.Preparser (
  allOperatorDefs
  , parseOperators
  , pOperatorDef
  , UnaryAssoc(..)
  , unaryAssoc
  , BinaryAssoc(..)
  , binaryAssoc
  , OperatorDef(..)
  , OperatorArity(..)
  , OperatorSym
  , arity
  , opSymbol
  , prec
  ) where

import           Common
import           Curry.Frontend.Haskelly.Parser.Base
import           Text.Megaparsec                     (MonadParsec, Parsec)
import qualified Text.Megaparsec                     as M

data UnaryAssoc = PrefixOp | PostfixOp
  deriving (Show)
data BinaryAssoc = None | LeftAssoc | RightAssoc
  deriving (Show)


data OperatorArity =
  Unary {
  _unaryAssoc ∷ UnaryAssoc
  }
  | Binary {
   _binaryAssoc ∷ BinaryAssoc
  }
  deriving (Show)
makeLenses ''OperatorArity

data OperatorDef =
  OperatorDef {
  _arity      ∷ OperatorArity
  , _opSymbol ∷ OperatorSym
  , _prec     ∷ Int
  }
  deriving (Show)
makeLenses ''OperatorDef

data KwInfix =
  KwInfix
  | KwInfixL
  | KwInfixR
  | KwPostfix
  | KwPrefix
  deriving (Show)

kwOp ∷ MonadParsec e Text m ⇒ m KwInfix
kwOp =
  KwInfixR <$ symbol' "infixr"
  <|> KwInfixL <$ symbol' "infixl"
  <|> KwInfix <$ symbol' "infix"
  <|> KwPrefix <$ symbol' "prefix"
  <|> KwPostfix <$ symbol' "postfix"

stuff ∷ MonadParsec e Text m ⇒ m ()
stuff = do
  M.notFollowedBy kwOp
  void . lexeme $ M.takeWhile1P Nothing (not . isSpace)

pOperatorDef ∷ ∀ m e. MonadParsec e Text m ⇒ m OperatorDef
pOperatorDef = M.label "operator definition" $ do
  kw <- kwOp
  aux $ case kw of
    KwInfix   → Binary None
    KwInfixR  → Binary RightAssoc
    KwInfixL  → Binary LeftAssoc
    KwPostfix → Unary PostfixOp
    KwPrefix  → Unary PrefixOp
  where
  aux ∷ OperatorArity → m OperatorDef
  aux a = uncurry (OperatorDef a) <$> symPrec
  symPrec ∷ m (OperatorSym, Int)
  symPrec = (,) <$> operatorSym <*> decimal

pOperatorDef' ∷ MonadParsec e Text m ⇒ m OperatorDef
pOperatorDef' = pOperatorDef <* many stuff

pOperators ∷ MonadParsec e Text m ⇒ m [OperatorDef]
pOperators = many pOperatorDef'

allOperatorDefs ∷ Parsec Void Text [OperatorDef]
allOperatorDefs = space >> many stuff >> pOperators <* M.eof

parseOperators ∷ Text → Either Text [OperatorDef]
parseOperators input = case M.parse allOperatorDefs "" input of
  Left e  → throwError (pack (M.errorBundlePretty e))
  Right r → return r
