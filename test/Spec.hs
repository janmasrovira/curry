module Main where

import           Common
import           Curry.Frontend.Haskelly.Parser
import           Curry.Typechecker
import qualified Data.Text.IO                   as T
import           Test.Tasty
import           Test.Tasty.HUnit

main ∷ IO ()
main = do
  defaultMain tests

tests ∷ TestTree
tests = testGroup "Tests" [unitTests]

parseAndCheck ∷ FilePath → Assertion
parseAndCheck path = do
  r <- parseModule <$> T.readFile path
  case r of
    Left err → assertFailure ("Parse error:" <> unpack err)
    Right m →
      case runCheckModule m of
        Left err  → assertFailure ("Typechecking error:" <> unpack err)
        Right{} → return ()

testFile ∷ FilePath → String → TestTree
testFile path testName =
  testCase testName (parseAndCheck path)

testFiles ∷ [(FilePath, String)]
testFiles = map (first ("test-files/" <>))
  [
    ("bool.cu", "Booleans and operations")
    , ("nat.cu", "Naturals and operations")
    , ("list.cu", "Lists and operations")
    , ("operators.cu", "Operators")
    , ("string.cu", "String literals")
  ]

unitTests ∷ TestTree
unitTests = testGroup "Unit tests"
  (map (uncurry testFile) testFiles)
